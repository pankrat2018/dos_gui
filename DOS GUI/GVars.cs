﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DOS_GUI
{
    class GVars
    {
        public static object Lock = new object();
        public static string Site;
        public static bool Work;
        public static string[] Proxies;
        public static int ProxyIndex;
        public static int ProxyType;
        public static bool Log;
        public static string StartTime;
    }
}
