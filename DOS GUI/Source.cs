﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DOS_GUI
{
    class Source
    {
        public static bool LoadProxies()
        {
            OpenFileDialog opd = new OpenFileDialog();
            opd.DefaultExt = ".txt";
            opd.Filter = "Текстовые файлы (.txt)|*.txt";
            if (opd.ShowDialog() == DialogResult.OK)
            {
                List<string> source = File.ReadAllLines(opd.FileName).ToList();
                source = source.Distinct().ToList();
                GVars.Proxies = source.Distinct().ToArray();
                return true;

            }
            return false;

        }
        public static string GetProxy()
        {
            lock (GVars.Lock)
            {
                if (GVars.ProxyIndex >= GVars.Proxies.Length)
                    GVars.ProxyIndex = 0;

                GVars.ProxyIndex++;

                return GVars.Proxies[GVars.ProxyIndex];
            }
        }
    }
}
